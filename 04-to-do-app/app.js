const argv = require('./config/yargs').argv;
const colors = require('colors');
const toDo = require('./to-do/to-do')

// console.log(argv);

let comando = argv._[0];

switch(comando){
    case 'crear':
        let task = toDo.crear( argv.descripcion );
        console.log(task); //node app crear -d "hola"
    break;

    case 'listar':
        let list = toDo.getList(); //node app listar

        for (let task of list) {
            console.log('========To Do============='.green);
            console.log(task.descripcion);
            console.log('Status: ', task.completado)
            console.log('=========================='.green);
        }

    break;

    case 'actualizar':
        let completed = toDo.update(argv.descripcion, argv.completado);
        console.log(completed); //node app actualizar -d "perrito" c true
    break;

    case 'borrar':
        let deleted = toDo.deleteTask(argv.descripcion);
        console.log(deleted)
    break;

    default:
    console.log('comando no es reconocido');
}
