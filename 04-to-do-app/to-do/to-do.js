const fs = require('fs');

let toDoList = [];

let saveDb = () => {
    let data = JSON.stringify(toDoList);

    fs.writeFile(`db/data.json`,data, (err) => {
        if (err) throw err;
        console.log(`se creó el archivo base.txt`);
    });
}

loadDb = () => {

    try {

        toDoList = require('../db/data.json');

    }catch (error) {

        toDoList = [];
    }
}

const crear = (descripcion) => {

    loadDb();

    let toDo = {
        descripcion,
        completado: false
    }

    toDoList.push(toDo);

    saveDb();

    return toDo;
}

const getList = () => {

    // return list = require('../db/data.json'); //my solution

    // fernando's solution
    loadDb();
    return toDoList;
}

const update = (description, completed = true) => {
    loadDb();

    let index = toDoList.findIndex( tarea => {
        return tarea.descripcion === description
    })

    if(index >= 0) {
        toDoList[index].completado = completed;
        saveDb();
        return true;
    } else {
        return false;
    }
}

const deleteTask = (description) => {
    loadDb();

    let tareaPorBorrar = toDoList.filter( tarea =>{
        return tarea.descripcion === description;
    })

    if(tareaPorBorrar){
        toDoList.splice(tareaPorBorrar,1);
        saveDb();
        return true
    }else{
        return false;
    }
    //hacer filtro de app listar por completados true o false
}

module.exports = {
    crear,
    getList, 
    update,
    deleteTask
}