const opts = {
    descripcion: {
        demand: true,
        alias: 'd',
        desc: 'Descripcion de la tarea por hacer'
    },
    completado: {
        alias: 'c',
        default: true,
        desc: 'marca como completado o pendiente la tarea'
    }
}

const argv = require('yargs')
    .command('crear', 'Crear un elemento por hacer', opts)
    .command('actualizar','Actualiza el estado completado',opts)
    .command('borrar','Elimina una tarea',opts)
    .help() // tipear en consola node app listar --help
    .argv;

module.exports = {
    argv
}