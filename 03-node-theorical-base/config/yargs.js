const opts = {
    base: {
        demand: true,
        alias: 'b',
    },
    limite: {
        alias: 'l',
        default: 10
    }
}

const argv = require('yargs')
    .command('listar','imprime en consola la tabla de multiplicar',opts)
    .command('crear', 'genera un archivo con la base de multplicar', opts)
    .help() // tipear en consola node app listar --help
    .argv;

module.exports = {
    argv
}