const fs = require('fs');
const colors = require('colors');

let listarTabla = (base, limite = 10) => {

    console.log('--------------------------'.green);
    console.log(`-----tabla del ${base}----`.green);
    console.log('--------------------------'.green);

    for(let i = 1; i <= limite; i++){
        console.log(base + " * " + i + " = " + base * i+ "\n" );
    }
}

// module.exports.crearArchivo = () => {
let crearArchivo = (base, limite =10) => {

    if( !Number(base)){
        reject(`${base} no es un numero`);
        return;
    }

    return new Promise((resolve, reject)=>{
        let data ='';

        for(let i = 1; i <= limite; i++){
        data += base + " * " + i + " = " + base * i+ "\n" ;
        }

        fs.writeFile(`tablas/tabla-${ base }-hasta${limite}.txt`,data, (err) => {
            if (err) throw err;
            console.log(`se creó el archivo tabla-${ base }-hasta${limite}!`.green);
        });
    })   
}

module.exports = {
    crearArchivo,
    listarTabla
}