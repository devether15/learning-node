let nombre = 'wolverine';

if( true ) {
    let nombre = 'magneto'; //cunado se usa let
}

// let nombre = 'wolverine1'; //tira erroe porque ocn let no puedo inicializar la variable dos veces
// let nombre = 'wolverine2';
// let nombre = 'wolverine3';
// let nombre = 'wolverine4';

// nombre = 'wolverine1'; 
// nombre = 'wolverine2';
// nombre = 'wolverine3';
// nombre = 'wolverine4'; //puedo reasignarla, pero no reinicializarla

console.log(nombre); //imprime wolverine porque la variable que está dentro del if tiene otro ámbito,si le sacamos el el a la variable en el if, imprime magneto porque si se puede reasignar mas no reinicializar


//en el caso de los for con var
for (var i = 0; i <= 5; i++) {
    console.log(`i: ${ i }`);    
}

console.log(i); // salida: 6, esto es porque como se declaró con var esta en un ambito global y el conteo quedó guardado

//en el caso de los for con let

let j = 'hola mundo';

for (let j = 0; j <= 5; j++) {
    console.log(`j: ${ j }`);    
}

console.log(j); //Hola mundo,