/**
 *  Async - Await
 */

//  let getNombre = async() => { //con la palabra async, ya se transforma la funcion getNombre() en una promesa
     
//     //undefined.nombre; //esto forzará un error
//     //throw new Error('No existe un nombrepara ese usuario'); //esto en caso de queramos especificar más el mensaje de error

//     return 'Fernando' //el return es ahora una promesa
//  }

 let getNombre = () => {
     return new Promise((resolve,reject) =>{

        setTimeout( () => {
            resolve('fernando')
        }, 3000);
     });
 }

 let saludo = async () => {

    let nombre = await getNombre();

     return `hola ${nombre}`
 }

 //console.log(getNombre());

 //como ahora es una promesa, podemos usar then y catch para resolver o capturar errrores según sea el caso
//  getNombre().then( nombre => {
//      console.log(nombre)
//  })
//  .catch(e => {
//      console.log('error de Async', e);
//  })

saludo().then(mensaje => {
    console.log(mensaje);
})