let nombre = 'Deadpool';
let real = 'Wade Winston';

//forma vieja de concatenación:
console.log(nombre + ' ' + real);

//usando template literals
console.log(`${ nombre} ${ real }`);

//sintacticamente y de tipo son iguales
let nombreCompleto = nombre + ' ' + real;
let nombreTemplate = `${ nombre } ${ real }`;
console.log( nombreCompleto === nombreTemplate);

//se puede usar en funciones
function getNombre() {
    return `${ nombre } ${ real }`;
}

//puedo inyectar el llamado de dicha funcion
console.log(`El nombre de: ${ getNombre() }`);

