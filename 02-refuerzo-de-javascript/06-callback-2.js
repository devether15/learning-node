let empleados = [{
    id:1,
    nombre:'Fernando'
},{
    id:2,
    nombre: 'Joseph'
},{
    id:3,
    nombre: 'Melisa'
}]

let salarios = [{
    id: 1,
    salario: 1000
},{
    id:2,
    salario: 2000
}]

let getEmpleado = (id, callback) => {
    let empleadoDb = empleados.find(empleado => empleado.id === id)

    if(!empleadoDb){
        callback(`No existe un empleado con el ID ${ id }`)
    }else{
        callback(null, empleadoDb);
    }
}

let getSalario = (empleado, callback) => {
    let salarioDb = salarios.find(salario => salario.id === empleado.id);

    if(!salarioDb){
        callback(`no existe un salario para el usuario ${empleado.nombre}`);
    } else{
        callback(null, {
            nombre: empleado.nombre,
            salario: salarioDb.salario,
            id: empleado.id
        });
    }
}


getEmpleado(1, (err,empleado) => {
    if(err){
        console.log(err);
    }

    getSalario( empleado, (err, resp) => {
        if(err){
            console.log(err);
        }
    
        console.log(`El salario de ${ resp.nombre } es de ${ resp.salario }`);
    })
})



