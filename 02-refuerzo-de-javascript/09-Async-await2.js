let empleados = [{
    id:1,
    nombre:'Fernando'
},{
    id:2,
    nombre: 'Joseph'
},{
    id:3,
    nombre: 'Melisa'
}]

let salarios = [{
    id: 1,
    salario: 1000
},{
    id:2,
    salario: 2000
}]

let getEmpleado = async (id) => {  //cuando cambiamos de promesa a async, agregamos la palabra reservada entes del parametro

        let empleadoDb = empleados.find(empleado => empleado.id === id)
    
        if(!empleadoDb){
            throw new Error(`No existe un empleado con el ID ${ id }`) //se cambia el reject por un throw new error
        }else{
            return empleadoDb; //se cambia el resolve por un return
        }

}

let getSalario = async (empleado) => {

        let salarioDb = salarios.find(salario => salario.id === empleado.id);
    
        if(!salarioDb){
            throw new Error(`no existe un salario para el usuario ${empleado.nombre}`);
        } else{
            return({
                nombre: empleado.nombre,
                salario: salarioDb.salario,
                id: empleado.id
            });
        }   

}

let getInformacion = async (id) =>{
    let empleado = await getEmpleado(id)
    let resp = await getSalario(empleado)

    return `${ resp.nombre} tiene un salario de ${resp.salario}$`;
}

getInformacion(2)
    .then(mensaje => console.log(mensaje))
    .catch(err => console.log(err));
