// function sumar (a,b) {
//     return a + b;
// }

// let sumar = (a,b) => { //se reemplaza la palabra function por let, se agrga el igual antes de los parametro y se agraga la felcha luesgo de los parámetros
//     return a + b;
// }

let sumar = (a,b) => a + b; //también se puede eliminar las llaves y el return

console.log(1,sumar(12, 18));

//ejercicio
// function saludar() {
//     return `Hola mundo`;
// }

let saludar = () => `Hola mundo`; //mi solucion

console.log(2, saludar());


//otro ejemmplo
// function saludar2 (nombre) {
//     return `hola ${ nombre }`;
// }

//let saludar2 = (nombre) => `Hola ${ nombre }`; se puedne quitar los parentesis, pero muchos deciden dejarlo para que sea mas claro que son los argumentos de la funcion
let saludar2 = nombre => `Hola ${ nombre }`;

console.log(3, saludar2('jose'));

//otro ejemplo, funcion flecha en los métodos de un objeto
// let  deadpool = {
//     nombre: 'Wade',
//     apellido: 'Winston',
//     poder: 'Regeneración',
//     getNombre: function() {
//         return `${ this.nombre } ${ this.apellido } - poder: ${ this.poder }`
//     }
// }

// let  deadpool = {
//     nombre: 'Wade',
//     apellido: 'Winston',
//     poder: 'Regeneración',
//     getNombre: () => {
//         return `${ this.nombre } ${ this.apellido } - poder: ${ this.poder }` //el this acá apunta al objeto window porque las funciones de flecha no generan contexto propio
//     }
// }

let  deadpool = {
    nombre: 'Wade',
    apellido: 'Winston',
    poder: 'Regeneración',
    getNombre () {
        return `${ this.nombre } ${ this.apellido } - poder: ${ this.poder }` //el this acá apunta al objeto window porque las funciones de flecha no generan contexto propio
    }
}

console.log(4, deadpool.getNombre());