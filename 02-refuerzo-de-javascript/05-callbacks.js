setTimeout( () => {
    console.log('hola mundo');
}, 3000);

let getUserById = (id, callback) => {
    let user = {
        name:'Joseph',
        id
    }

    if(id === 20) {
        callback(`el usuario con ide ${ id }, no existe`);
    } else {
        callback(user);        
    }

}

getUserById(10, (err, user) => {
    if( err ){
        return console.log(err);
    }
    console.log('user in data base', user);
})